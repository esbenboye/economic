<?php
namespace Economic;

class QueryBuilder{
    private $filters = [];
    private $params = [];

    private $converters = [
        "="     => '$eq:',
        "!="    => '$ne:',
        ">"     => '$gt:',
        ">="    => '$gte:',
        "<"     => '$lt:',
        "<="    => '$lte:',
        "like"  => '$like:',
        "in"    => '$in:'
    ];

    public function where($field, $type, $value = null){
        if(is_null($value)){
            $value = $type;
            $type = "=";
        }

        $type = strtolower($type);

        if(isset($this->converters[$type])){
            if(is_array($value)){
                $value = json_encode($value);
            }

            $type = $this->converters[$type];
            $this->filters[] = $field.$type.$value;
            return $this; // Make it chainable
        }else{
            throw new Exception("Comparitor [$type] is not supported");
        }
    }

    public function whereIn($key, $values){
        return $this->where($key, 'in', $values);
        return $this;
    }

    public function whereLike($key, $value){
        return $this->where($key, 'like', $value);
    }

    public function orderBy($key, $value){
        $value = strtolower($value);
        if($value == 'asc'){
            $this->params['sort'] = "$key";
        }else{
            $this->params['sort'] = "-$key";
        }
        return $this;
    }
    
    public function buildUrlParams(){
        $params = [];

        if(count($this->filters) > 0){
            $filter = implode('$and:', $this->filters);
            $params['filter'] = $filter;
        }

        foreach($this->params as $key => $value){
            $params[$key] = $value;
        }

        return http_build_query($params);
    }

    public function page($page){

        $page = intval($page);
        if($page === false){
            throw new Exception("Page must be an integer");
        }
        if($page < 0){
            throw new Exception("Page cannot be less than zero");
        }
        $this->params['skippages'] = $page-1;
        return $this;
    }

    public function pageSize($size){
        $size = intval($size);
        if($size === false){
            throw new Exception("Size must be an integer");
        }
        $this->params['pagesize'] = $size;
    }
}

<?php

namespace Economic\Invoice;
use Economic\Base;
use Economic\Invoice\Line;

class Draft extends Base{
    protected $endpoint = "/invoices/drafts";
}

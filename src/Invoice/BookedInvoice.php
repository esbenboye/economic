<?php

namespace Economic\Invoice;

use Economic\Invoice;
use Economic\Base;

class BookedInvoice extends Invoice{
    protected $endpoint = "/invoices/booked";
    protected $identifier = 'bookedInvoiceNumber';
}

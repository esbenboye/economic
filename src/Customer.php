<?php

namespace Economic;

class Customer extends Base{
    protected $endpoint = "customers";
    protected $identifier = "customerNumber";

    private $filterValue = 'customer.customerNumber';

    public function Contacts(){
        return $this->queryRelation(\Economic\Customer\Contact::class);
    }

    public function BookedInvoices(){
        return $this->whereRelation(\Economic\Invoice\BookedInvoice::class, $this->filterValue);
    }

    public function Drafts(){
    	return $this->whereRelation(\Economic\Invoice\Draft::class, $this->filterValue);
    }
}

<?php

namespace Economic;

use Economic\Exception\NotFoundException;
use Economic\QueryBuilder;
use Economic\Exception;
use Economic\Connection;
use Economic\Exception\HttpException;

class Base{

    protected $connection;
    protected $identifier;

    private $browser;
    private $filters = array();
    public $pageSize = 20;

    protected $parentIdentifier = null;
    private $parentIdentifierValue = null;


    public function __get($function){
        if(method_exists($this, $function)){
            $obj = $this->$function();
            return $obj;
        }
    }

    public function __construct($data = null){
        if(!is_null($data)){
            $this->bind($data);
        }

        $this->connection = Connection::getInstance();
        $this->newQueryBuilder();
    }

    // Making it possible to call methods directly on the QueryBuilder object.
    // Could have implemented the functions directly into the Base object,
    // but I wanted to keep the queryBuilder and the Base object as two seperate
    // objects and to keep it possible to (easily) reset this->queryBuilder and/or
    // change the QueryBuilder to another class if needs be.

    public function __call($func, $args){
        if(method_exists($this->queryBuilder, $func)){
            call_user_func_array([$this->queryBuilder, $func], $args);
            return $this;
        }
    }

    public function bind($data){
        foreach($data as $key => $value){
            $this->$key = $value;
        }
    }

    public function delete($id, $parentIdentifierValue = null){

        $this->setPrimaryIdentifierValue($id);
        $this->setParentIdentifierValue($parentIdentifierValue);
        
        $url = $this->buildUrl();
        $response = $this->connection->delete($url);
        return $response;
    }

    public function find($id, $parentIdentifierValue = null){

        $this->setPrimaryIdentifierValue($id);
        $this->setParentIdentifierValue($parentIdentifierValue);
        
        $url = $this->buildUrl();
        $response = $this->connection->get($url);

        try{
            $this->checkResponse($response);
        }catch(NotFoundException $e){
            return null;
        }

        $this->bind($response);
        return $this;
    }

    public function get($parentIdentifierValue = null){

        $this->setParentIdentifierValue($parentIdentifierValue);

        $url = $this->buildUrl();
        $response = $this->connection->get($url);
        $this->checkResponse($response);
        $class = get_class($this);
        $collection = new Collection();
        $collection->bind($response, $class, $this->parentIdentifierValue);
        return $collection;
    }

    protected function create($parentIdentifierValue = null){
        if(!is_null($parentIdentifierValue)){
            $this->setParentIdentifierValue($parentIdentifierValue);
        }

        $url = $this->buildUrl();
        $response = $this->connection->post($url, $this->toEcData());
        $this->checkResponse($response);
        $this->bind($response);
        return $this;
    }

    public function save($parentIdentifierValue = null){
        if(!is_null($parentIdentifierValue)){
            $this->setParentIdentifierValue($parentIdentifierValue);
        }

        if(!$this->{$this->identifier}){
            return $this->create();
        }

        $url = $this->buildUrl();
        $response = $this->connection->put($url, $this->toEcData());
        $this->checkResponse($response);
        $this->bind($response);

        return $this;
    }

    protected function checkParentIdentifier(){
        if(!is_null($this->parentIdentifier)){
            if(is_null($this->parentIdentifierValue)){
                throw new Exception("{$this->parentIdentifier} must be set");
            }
        }
    }

    public function toEcData(){
        return json_encode($this);
    }

    public function newQueryBuilder(){
       $this->queryBuilder = new QueryBuilder();
    }

    public function buildUrl(){
        $endpoint = $this->buildEndpoint();
        $url = $this->connection->getUrl()."/".$endpoint;
        $url.="?".$this->queryBuilder->buildUrlParams();
        return $url;
    }

    protected function buildEndpoint(){
        if(is_null($this->parentIdentifier)){
            $endpoint = $this->endpoint;
        }else{
            $endpoint = $this->replaceParentIdentifier();
        }

        if(isset($this->{$this->identifier})){
            $endpoint.="/".$this->{$this->identifier};
        }

        return $endpoint;
    }

    protected function replaceParentIdentifier(){
        $this->checkParentIdentifier();
        return str_replace($this->parentIdentifier, $this->parentIdentifierValue, $this->endpoint);
    }

    protected function checkResponse($response){
       if(isset($response->httpStatusCode)){
           switch($response->httpStatusCode){
                case 404:
                    throw new NotFoundException($response->message);
                break;
                default:
                    throw new HttpException($response->message, $response->httpStatusCode);
                break;
           }
       }
    }

    public function getPrimaryIdentifierField(){
        return $this->identifier;
    }

    public function getPrimaryIdentifierValue(){
        $field = $this->getPrimaryIdentifierField();
        return $this->$field;
    }

    public function setPrimaryIdentifierValue($value){
        $field = $this->getPrimaryIdentifierField();
        $this->$field = $value;
    }

    public function setParentIdentifierValue($value){
        if(!is_null($value)){
            $this->parentIdentifierValue = $value;    
        }
    }

    public function queryRelation($class){
        $obj = new $class();
        $obj->setParentIdentifierValue($this->getPrimaryIdentifierValue());
        return $obj;
    }

    public function whereRelation($class, $whereIdentifier){
        $obj = new $class();
        $obj->where($whereIdentifier, $this->getPrimaryIdentifierValue());
        return $obj;
    }
}

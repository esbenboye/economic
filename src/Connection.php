<?php
namespace Economic;
use \Buzz\Browser;
use \Buzz\Client\Curl;

class Connection{
	protected $url = "https://restapi.e-conomic.com";
    public static $instance;

    public $headers = [
        "X-AppSecretToken" => '',
        "X-AgreementGrantToken" => '',
        "Content-Type" => "application/json"
    ];

    public function __construct(){
        $this->browser = new Browser(new Curl());
        $this->browser->getClient()->setTimeout(120);
    }

    public function get($url, $headers = null){
        if(is_null($headers)){
            $headers = $this->headers;
        }

        $response = $this->browser->get($url, $headers);
        return $this->response($response);

    }

    public function put($url, $data, $headers = null){
        if(is_null($headers)){
            $headers = $this->headers;
        }
        $response = $this->browser->put($url, $headers, $data);
        return $this->response($response);
    }

    public function post($url, $data, $headers = null){
        if(is_null($headers)){
            $headers = $this->headers;
        }
        $response = $this->browser->post($url, $headers, $data);
        return $this->response($response);
    }

    public function delete($url, $headers = null){
        if(is_null($headers)){
            $headers = $this->headers;
        }

        $response = $this->browser->delete($url, $headers);
		return $this->response($response);
    }

	public function response($response){
		if(strpos($response->getHeader('Content-Type'), 'json') !== false){
            return json_decode($response->getContent());
        }else{
            return $response->getContent();
        }
	}

    public function setApptoken($apptoken){
        $this->headers['X-AppSecretToken'] = $apptoken;
        return $this;
    }
    public function setGranttoken($granttoken){
        $this->headers['X-AgreementGrantToken'] = $granttoken;
        return $this;
    }
    public function setUrl($url){
        $this->url = $url;
        return $this;
    }

    public static function getInstance(){
        if(is_null(self::$instance)){
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getUrl(){
        return $this->url;
    }

}

<?php

namespace Economic;

class Invoice extends Base{
    protected $endpoint = "/invoices";
    protected $identifier = '';

    public function getRawPdf(){
        $identifier = $this->{$this->identifier};
        $url = $this->connection->url.$this->endpoint."/".$identifier."/pdf";
        return $this->connection->get($url);
    }
}

<?php

namespace Economic;

use stdClass;
use ArrayIterator;

class Collection extends ArrayIterator{
    private $class;

    public function bind($response, $class, $parentIdentifierValue = null){
        $this->class = $class;
        $this->clear();
        if(isset($response->collection) && !is_null($response->collection)){
            foreach($response->collection as $data){
                $model = new $this->class($data);
                $model->setParentIdentifierValue($parentIdentifierValue);
                $this[] = $model;
            }
        }
        $this->bindPagination($response);
        return $this;
    }

    private function bindPagination($response){
        $this->pagination = new stdClass();
        $this->pagination->maxPageSizeAllowed = $response->pagination->maxPageSizeAllowed;
        $this->pagination->skipPages = $response->pagination->skipPages;
        $this->pagination->pageSize = $response->pagination->pageSize;
        $this->pagination->results = $response->pagination->results;
        $this->pagination->resultsWithoutFilter = $response->pagination->resultsWithoutFilter;
        $this->pagination->totalPages = ceil($this->pagination->resultsWithoutFilter/$this->pagination->pageSize);
    }

    private function clear(){
        foreach(iterator_to_array($this) as $key => $value){
            unset($this[$key]);
        }
        return $this;
    }
}

<?php

namespace Economic;

class Currency extends Base{
	protected $endpoint = "currencies";
	protected $identifier = "code";
}
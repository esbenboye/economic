<?php

namespace Economic\Customer;

use Economic\Base;

class Contact extends Base{
    protected $endpoint = "customers/:customerNumber/contacts";
    protected $identifier = "customerContactNumber";
    protected $parentIdentifier = ":customerNumber";
}
